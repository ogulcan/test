//
//  AppCoordinator.swift
//  case
//
//  Created by Oğulcan on 12-02-2022.
//

import UIKit

class AppCoordinator: Coordinator {
    
    private let window: UIWindow
    
    var childCoordinators: [Coordinator] = [Coordinator]()
    var navigationController: UINavigationController
    
    init(window: UIWindow) {
        self.window = window
        self.navigationController = UINavigationController()
    }
    
    func initiate() {
        window.rootViewController = self.navigationController
        window.rootViewController?.view.backgroundColor = .white
        
        let homeCoordinator = HomeCoordinator(navigationController: navigationController)
        childCoordinators = [homeCoordinator]
        homeCoordinator.initiate()
    }
}
