//
//  Coordinator.swift
//  case
//
//  Created by Oğulcan on 12-02-2022.
//

import UIKit

protocol Coordinator: AnyObject {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }

    func initiate()
}

protocol Router: AnyObject {
    
}
