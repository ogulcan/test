//
//  HomeCoordinator.swift
//  case
//
//  Created by Oğulcan on 12-02-2022.
//

import Foundation
import UIKit

class HomeCoordinator: Coordinator, HomeRouter {
    
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func initiate() {
        let networkService = NetworkService()
        let homeUseCase = HomeUseCase(networkService: networkService)
        let homeViewModel = HomeViewModel(useCase: homeUseCase)
        let homeViewController = HomeViewController(router: self, viewModel: homeViewModel)
        navigationController.setViewControllers([homeViewController], animated: true)
    }
    
    func showImageDetail(with image: UIImage) {
        let detailViewController = DetailViewController(image: image)
        navigationController.pushViewController(detailViewController, animated: true)
    }
    
    func showDetail(with data: Data) {
        fatalError("Not implemented")
    }
}

protocol HomeRouter: Router {
    func showImageDetail(with image: UIImage)
    func showDetail(with data: Data)
}
