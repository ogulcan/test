//
//  NetworkService.swift
//  case
//
//  Created by Oğulcan on 12-02-2022.
//

import Foundation
import Combine
import UIKit

class NetworkService {

    static var concurrentScheduler: OperationQueue = {
        let operationQueue = OperationQueue()
        operationQueue.maxConcurrentOperationCount = 3
        operationQueue.qualityOfService = QualityOfService.userInitiated
        return operationQueue
    }()
    
    private let session: URLSession
    private let sessionConfiguration: URLSessionConfiguration
    var subscriptions = [AnyCancellable]()
    
    init() {
        #if DEBUG
        self.sessionConfiguration = URLSessionConfiguration.ephemeral
        #else
        self.sessionConfiguration = URLSessionConfiguration.default
        #endif
        self.session = URLSession(configuration: sessionConfiguration)
    }
    
    func load<T>(_ requestable: Requestable<T>) -> AnyPublisher<T, Error> {
        guard let request = requestable.request else {
            return Fail(error: NetworkError.network(description: "Could not create request")).eraseToAnyPublisher()
        }
        
        return session.dataTaskPublisher(for: request)
            .mapError { error in NetworkError.network(description: error.localizedDescription) }
            .flatMap(maxPublishers: .max(1)) { pair in
                decode(pair.data)
            }
            .eraseToAnyPublisher()
    }
    
    func loadImage(from url: URL) -> AnyPublisher<ContentRepresentable, Never> {
        // print("Start")
        return URLSession.shared.downloadTaskPublisher(for: url)
            .map { URL, response in
                let image = UIImage(contentsOfFile: URL.path)
                let size = URL.fileSize()
                return ContentRepresentable(imageUrl: URL.path, image: image, size: size)
            }.catch { error in
                return Just(ContentRepresentable(imageUrl: "", image: nil, size: 0))
            }.eraseToAnyPublisher()
    }
}

enum NetworkError: Error {
    case parsing(description: String)
    case network(description: String)
}
