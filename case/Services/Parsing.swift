//
//  Parsing.swift
//  case
//
//  Created by Oğulcan on 12-02-2022.
//

import Foundation
import Combine

func decode<T: Decodable>(_ data: Data) -> AnyPublisher<T, Error> {
    let decoder = JSONDecoder()
    decoder.dateDecodingStrategy = .secondsSince1970
    
    return Just(data)
        .decode(type: Response<T>.self, decoder: decoder)
        .mapError { error in
            print(error)
            return NetworkError.parsing(description: error.localizedDescription)
        }.map({ response in response.result })
        .eraseToAnyPublisher()
}
