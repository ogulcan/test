//
//  Request.swift
//  case
//
//  Created by Oğulcan on 12-02-2022.
//

import Foundation

struct Requestable<T: Decodable> {
    
    let url: URL?
    
    let params: [String: String]
    var request: URLRequest? {
        guard let url = url else { return nil }
        guard var components = URLComponents(url: url, resolvingAgainstBaseURL: false) else { return nil }
        components.queryItems = params.map {
            URLQueryItem(name: $0.key, value: $0.value)
        }
        guard let url = components.url else { return nil }
        return URLRequest(url: url)
    }
    
    init(url: URL?, params: [String: String] = [:]) {
        self.url = url
        self.params = params
    }
}
