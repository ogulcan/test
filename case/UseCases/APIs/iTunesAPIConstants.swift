//
//  iTunesAPI.swift
//  case
//
//  Created by Oğulcan on 12-02-2022.
//

import Foundation

struct iTunesSearchAPI: CaseAPI {

    var scheme = "https"
    var host = "itunes.apple.com"
    var path = "/search"
    
    func getURL() -> URL? {
        var components = URLComponents()
        
        components.scheme = scheme
        components.host = host
        components.path = path
        
        return components.url
    }
}
protocol CaseAPI {
    var scheme: String { get set }
    var host: String { get set }
    var path: String { get set }
    
    func getURL() -> URL?
}

enum MediaType: String {
    case movie = "movie"
    case podcast = "podcast"
    case music = "music"
    // ...
    case software = "software"
}
