//
//  HomeUseCase.swift
//  case
//
//  Created by Oğulcan on 12-02-2022.
//

import Foundation
import Combine
import UIKit

final class HomeUseCase {
    
    private let networkService: NetworkService
    private let api: CaseAPI
    
    init(networkService: NetworkService, api: CaseAPI = iTunesSearchAPI()) {
        self.networkService = networkService
        self.api = api
    }
    
    func search(for query: String, type: MediaType = .software) -> AnyPublisher<[Content], Error> {
        let params: [String: String] = [
            "term": query,
            "media": type.rawValue
        ]
        let requestable = Requestable<[Content]>(url: api.getURL(), params: params)
        
        return networkService.load(requestable)
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
    
    func loadImage(for url: String) -> AnyPublisher<ContentRepresentable, Never> {
        return Deferred {
            return Just(url)
        }.flatMap({ [unowned self] url -> AnyPublisher<ContentRepresentable, Never> in
            guard let url = URL(string: url) else { return Just(ContentRepresentable(imageUrl: "", image: nil, size: 0)).eraseToAnyPublisher() }
            return self.networkService.loadImage(from: url)
        })
        .receive(on: RunLoop.main)
        .eraseToAnyPublisher()
    }
}
