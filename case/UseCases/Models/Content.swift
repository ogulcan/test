//
//  Content.swift
//  case
//
//  Created by Oğulcan on 12-02-2022.
//

import Foundation
import UIKit

struct Content {
    let trackId: Int
    let trackName: String
    let sellerName: String
    let minVersion: String
    // let image: String
    let screenshots: [String]
    // ...
}

extension Content: Decodable {
    enum CodingKeys: String, CodingKey {
        case trackId
        case trackName
        case sellerName
        case minVersion = "minimumOsVersion"
        // case image = "artworkUrl512"
        case screenshots = "screenshotUrls"
    }
}

extension Content: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(trackId)
    }
}

struct ContentRepresentable {
    let imageUrl: String
    let image: UIImage?
    let size: Double
}

extension ContentRepresentable: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(size)
    }
    
    func getSectionBySize() -> String {
        if 0...100 ~= size {
            return "100"
        } else if 100...250 ~= size {
            return "250"
        } else if 250...500 ~= size {
            return "500"
        } else {
            return "500+"
        }
    }
}
