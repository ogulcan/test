//
//  Response.swift
//  case
//
//  Created by Oğulcan on 12-02-2022.
//

import Foundation

struct Response<T: Decodable> {
    let count: Int
    let result: T
}

extension Response: Decodable {
    enum CodingKeys: String, CodingKey {
        case count = "resultCount"
        case result = "results"
    }
}
