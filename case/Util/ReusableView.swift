//
//  ReusableView.swift
//  case
//
//  Created by Oğulcan on 13-02-2022.
//

import Foundation

protocol ReusableView {
    static var identifier: String { get }
}

extension ReusableView {
    static var identifier: String {
        return "\(self)"
    }
}
