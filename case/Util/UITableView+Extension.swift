//
//  UITableView+Extension.swift
//  case
//
//  Created by Oğulcan on 13-02-2022.
//

import Foundation
import UIKit

extension UITableView {
    func dequeueReusableCell<T: UITableViewCell>(withClass `class`: T.Type) -> T? where T: ReusableView {
        return self.dequeueReusableCell(withIdentifier: `class`.identifier) as? T
    }
}
