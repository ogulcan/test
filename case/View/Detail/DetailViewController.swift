//
//  DetailViewController.swift
//  case
//
//  Created by Oğulcan on 12-02-2022.
//

import UIKit

class DetailViewController: UIViewController {
    
    private let image: UIImage
    
    private lazy var imageView: UIImageView = {
        let _imageView = UIImageView()
        _imageView.translatesAutoresizingMaskIntoConstraints = false
        return _imageView
    }()
    
    private lazy var scrollView: UIScrollView = {
        let _scrollView = UIScrollView()
        _scrollView.translatesAutoresizingMaskIntoConstraints = false
        _scrollView.minimumZoomScale = 1.0
        _scrollView.maximumZoomScale = 3.0
        _scrollView.delegate = self
        _scrollView.backgroundColor = .black
        return _scrollView
    }()
    
    required init?(coder: NSCoder) {
        fatalError("Not implemented")
    }
    
    init(image: UIImage) {
        self.image = image
        super.init(nibName: nil, bundle: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Image Detail"
        view.backgroundColor = .black
        imageView.image = image
        addViewsAndConstraints()
    }
    
    func addViewsAndConstraints() {
        view.addSubview(scrollView)
        
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.topAnchor),
            scrollView.leftAnchor.constraint(equalTo: view.leftAnchor),
            scrollView.rightAnchor.constraint(equalTo: view.rightAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
        scrollView.addSubview(imageView)
    }
}

extension DetailViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}
