//
//  HomeTableViewCell.swift
//  case
//
//  Created by Oğulcan on 13-02-2022.
//

import UIKit

class HomeTableViewCell: UITableViewCell, ReusableView {
    
    private lazy var softwareNameLabel: UILabel = {
        let _titleView = UILabel()
        _titleView.translatesAutoresizingMaskIntoConstraints = false
        // _titleView.backgroundColor = .green
        return _titleView
    }()
    
    private lazy var screenshotImageView: UIImageView = {
        let _screenshotImageView = UIImageView()
        _screenshotImageView.translatesAutoresizingMaskIntoConstraints = false
        _screenshotImageView.layer.cornerRadius = 5
        _screenshotImageView.contentMode = .scaleAspectFit
        return _screenshotImageView
    }()

    required init?(coder: NSCoder) {
        fatalError("Not implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addViewsAndConstraints()
    }
    
    func bind(to content: Content) {
        print("Hello: \(content.trackName)")
        softwareNameLabel.text = "\(content.trackName), from: \(content.sellerName)"
    }
    
    func bind(to contentRepresentable: ContentRepresentable) {
        screenshotImageView.image = contentRepresentable.image
    }
    
    private func addViewsAndConstraints() {
        addSubview(softwareNameLabel)
        
        NSLayoutConstraint.activate([
            softwareNameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
            softwareNameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -30),
            softwareNameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            softwareNameLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10)
            //titleView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
        ])
        
        addSubview(screenshotImageView)
        NSLayoutConstraint.activate([
            screenshotImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
            screenshotImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            screenshotImageView.heightAnchor.constraint(equalToConstant: 30),
            screenshotImageView.widthAnchor.constraint(equalToConstant: 100)
        ])
    }
}
