//
//  TableStateView.swift
//  case
//
//  Created by Oğulcan on 12-02-2022.
//

import UIKit

class HomeInfoView: UIView {
    
    private lazy var messageLabel: UILabel = {
        let _messageLabel = UILabel()
        _messageLabel.translatesAutoresizingMaskIntoConstraints = false
        _messageLabel.font = UIFont.systemFont(ofSize: 20)
        _messageLabel.text = "No data"
        _messageLabel.textAlignment = .center
        _messageLabel.minimumScaleFactor = 0.5
        _messageLabel.numberOfLines = 1
        _messageLabel.adjustsFontSizeToFitWidth = true
        return _messageLabel
    }()
    
    var query: String? {
        didSet {
            messageLabel.text = "No data \(query ?? "")"
        }
    }
    
    init() {
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        addViewsAndConstraints()
    }
    
    func setQuery(_ query: String?) {
        self.query = query
    }
    
    func addViewsAndConstraints() {
        backgroundColor = .gray.withAlphaComponent(0.8)
        layer.borderWidth = 2
        layer.borderColor = UIColor.darkGray.cgColor
        layer.cornerRadius = 6
        
        addSubview(messageLabel)
        NSLayoutConstraint.activate([
            messageLabel.topAnchor.constraint(equalTo: topAnchor),
            messageLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            messageLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            messageLabel.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
    
    func show(with animation: Bool = true, autoDismiss: Bool = true) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            guard let superview = self.superview else { return }
            superview.bringSubviewToFront(self)
            UIView.animate(withDuration: 0.3, animations: {
                self.alpha = 1
            })
        }
        if autoDismiss {
            Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: { [weak self] _ in
                self?.hide()
            })
        }
    }
    
    func hide(with animation: Bool = true) {
        DispatchQueue.main.async { [weak self] in
            UIView.animate(withDuration: 0.3, animations: { [weak self] in
                self?.alpha = 0
            }, completion: { [weak self] _ in
                guard let self = self else { return }
                guard let superview = self.superview else { return }
                superview.sendSubviewToBack(self)
            })
        }
    }
}
