//
//  ViewController.swift
//  case
//
//  Created by Oğulcan on 12-02-2022.
//

import UIKit
import Combine

class HomeViewController: UIViewController {
    
    fileprivate weak var viewModel: HomeViewModel?
    private weak var router: HomeRouter?
    
    private lazy var dataSource = prepareDataSource()
    private lazy var snapshot = NSDiffableDataSourceSnapshot<Section, ContentRepresentable>()
    private var cancellables = Set<AnyCancellable>()

    lazy var searchBar: UISearchBar = {
        let _searchBar = UISearchBar()
        _searchBar.translatesAutoresizingMaskIntoConstraints = false
        _searchBar.placeholder = "Search"
        _searchBar.searchBarStyle = .minimal
        _searchBar.autocorrectionType = .no
        _searchBar.autocapitalizationType = .none
        _searchBar.delegate = self
        return _searchBar
    }()
    
    lazy var tableView: UITableView = {
        let _tableView = UITableView()
        _tableView.delegate = self
        _tableView.translatesAutoresizingMaskIntoConstraints = false
        _tableView.tableFooterView = UIView()
        _tableView.indicatorStyle = .black
        _tableView.separatorStyle = .singleLine
        _tableView.register(HomeTableViewCell.self, forCellReuseIdentifier: HomeTableViewCell.identifier)
        return _tableView
    }()
    
    lazy var activityIndicatorView: UIActivityIndicatorView = {
        let _activityIndicatorView = UIActivityIndicatorView(style: .medium)
        _activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        _activityIndicatorView.hidesWhenStopped = true
        _activityIndicatorView.stopAnimating()
        return _activityIndicatorView
    }()
    
    lazy var infoView: HomeInfoView = {
        let _infoView = HomeInfoView()
        _infoView.translatesAutoresizingMaskIntoConstraints = false
        _infoView.alpha = 0
        return _infoView
    }()
    
    required init?(coder: NSCoder) {
        fatalError("Not implemented")
    }
    
    init(router: HomeRouter?, viewModel: HomeViewModel) {
        self.router = router
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Case"
        addViewsAndConstraints()
        bindViewModelToView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    private func addViewsAndConstraints() {
        view.addSubview(searchBar)
        NSLayoutConstraint.activate([
            searchBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            searchBar.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            searchBar.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
        
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: searchBar.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
        ])
        
        view.addSubview(activityIndicatorView)
        NSLayoutConstraint.activate([
            activityIndicatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            activityIndicatorView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
        
        view.addSubview(infoView)
        view.sendSubviewToBack(infoView)
        NSLayoutConstraint.activate([
            infoView.heightAnchor.constraint(equalToConstant: 100),
            infoView.widthAnchor.constraint(equalToConstant: 200),
            infoView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            infoView.topAnchor.constraint(equalTo: view.topAnchor, constant: 200)
        ])
    }
    
    private func bindViewModelToView() {
        viewModel?.$state
            .receive(on: RunLoop.main)
            .sink(receiveValue: { [unowned self] state in
                switch state {
                    case .idle:
                        self.activityIndicatorView.stopAnimating()
                        self.resetDataSource()
                    case .loading:
                        self.resetDataSource()
                        self.activityIndicatorView.startAnimating()
                    case .success(let content):
                        self.activityIndicatorView.stopAnimating()
                        self.updateDataSource(with: content.first)
                    case .error(let error):
                        self.activityIndicatorView.stopAnimating()
                        print("ERROR \(error)")
                        // TODO: Show error message
                    case .noData:
                        self.infoView.setQuery(viewModel?.search)
                        self.activityIndicatorView.stopAnimating()
                        self.infoView.show()
                        break
                }
            }).store(in: &cancellables)
    }
}

extension HomeViewController: UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        searchBar.resignFirstResponder()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let section = Section.allCases[section]
        let label = UILabel()
        label.text = section.rawValue
        return label
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return tableView.dataSource?.tableView(tableView, numberOfRowsInSection: section) == 0 ? 0: 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let image = dataSource.itemIdentifier(for: indexPath)?.image else { return }
        self.router?.showImageDetail(with: image)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension HomeViewController {
    enum Section: String, CaseIterable {
        case kb100 = "100"
        case kb250 = "250"
        case kb500 = "500"
        case kb500plus = "500+"
    }
    
    func prepareDataSource() -> UITableViewDiffableDataSource<Section, ContentRepresentable> {
        return UITableViewDiffableDataSource(
            tableView: tableView,
            cellProvider: { tableView, indexPath, content in
                guard let cell = tableView.dequeueReusableCell(withClass: HomeTableViewCell.self) else {
                    return UITableViewCell()
                }
                
                cell.bind(to: content)
                return cell
            }
        )
    }
    
    func resetDataSource() {
        snapshot = NSDiffableDataSourceSnapshot<Section, ContentRepresentable>()
        snapshot.appendSections(Section.allCases)
        dataSource.apply(snapshot)
    }
    
    func updateDataSource(with content: ContentRepresentable?) {
        guard let content = content else { return }
        guard let _ = content.image else { return }
        
        snapshot.appendItems([content], toSection: Section(rawValue: content.getSectionBySize())!)
        dataSource.apply(snapshot)
    }
}

extension HomeViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel?.search = searchText
    }
}
