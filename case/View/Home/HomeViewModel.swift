//
//  HomeViewModel.swift
//  case
//
//  Created by Oğulcan on 12-02-2022.
//

import Foundation
import Combine
import UIKit

enum HomeViewModelState {
    case idle
    case loading
    case success([ContentRepresentable])
    case error(Error)
    case noData
}

class HomeViewModel: ObservableObject {
    
    @Published var search: String = ""
    @Published private(set) var state: HomeViewModelState = .idle
    
    private let useCase: HomeUseCase
    private var cancellable: AnyCancellable?
    private var cancellables = Set<AnyCancellable>()
    
    init(useCase: HomeUseCase, scheduler: DispatchQueue = DispatchQueue(label: "HomeViewModel")) {
        self.useCase = useCase
        
        cancellable?.cancel()
        cancellable = $search
            .filter({ [weak self] value in
                if (value.isEmpty) {
                    self?.state = .idle
                    self?.cancellables.forEach { $0.cancel() }
                    self?.cancellables.removeAll()
                }
                return !value.isEmpty
            })
            .debounce(for: .milliseconds(800), scheduler: scheduler)
            .sink(receiveValue: fetchSoftwares(for:))
    }
    
    func fetchSoftwares(for query: String) {
        cancellables.forEach { $0.cancel() }
        cancellables.removeAll()
        
        state = .loading
        
        let fetchQueryCompletion: (Subscribers.Completion<Error>) -> Void = { [weak self] completion in
            switch completion {
                case .failure(let failure):
                    self?.state = .error(failure)
                    break
                case .finished:
                    break
            }
        }
        
        let fetchQueryValueHandler: ([Content]) -> Void = { [weak self] content in
            guard let self = self else { return }
            if content.isEmpty {
                self.state = .noData
                return
            } else {
                let imageURLs = Array(content.map({ $0.screenshots }).joined())//[...15] // For a tiny demo
                print("Download: \(imageURLs.count) image")

                Publishers.Sequence(sequence: imageURLs.map { self.useCase.loadImage(for: $0) })
                     .flatMap(maxPublishers: .max(3)) { $0 }
                     .subscribe(on: NetworkService.concurrentScheduler)
                     .sink(receiveValue: { contentRepresentable in
                         // print("Done: \(contentRepresentable.size)")
                         self.state = .success([contentRepresentable])
                     }).store(in: &self.cancellables)
            }
        }
        
        useCase.search(for: query)
            .sink(receiveCompletion: fetchQueryCompletion, receiveValue: fetchQueryValueHandler)
            .store(in: &cancellables)
    }
}
